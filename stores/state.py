from domain.amr_status import AMRStatus
from domain.section_state import SectionState
from typing import Dict
import json
from agent.socketio_agent import SocketIOAgent
from service import section_state_service as SectionService 

class StatusStore:
    current_status: AMRStatus = AMRStatus()
    latest_status: AMRStatus = AMRStatus()
    section_status: Dict[int, bool] = {} # 0: true，store的沒有欄位名稱，與資料庫不太一樣，須注意
    road_state = {"floor1": False, "floor2": False, "floor3": False }

    @classmethod
    def broadcastAllStatusFromStore(cls):
        SocketIOAgent.updateSectionState(json.dumps(cls.section_status))
        SocketIOAgent.updateAMRState(json.dumps(cls.latest_status.__dict__))
        SocketIOAgent.updateRoadState(json.dumps(cls.road_state))

    @classmethod
    def initStore(cls):
        cls.section_status = SectionService.get_all()

    @classmethod
    def update_state_by_section_state_change(cls, section_state):
        if cls.section_status[section_state.section_id] != section_state.state:
            cls.section_status[section_state.section_id] = section_state.state
            json_data = json.dumps(cls.section_status)
            SocketIOAgent.updateSectionState(json_data)
            # print(json_data)

    @classmethod
    def update_state_by_ros_bridge_trigger(cls):
        if StatusStore.current_status.__dict__ != StatusStore.latest_status.__dict__:
            cls.latest_status.update(cls.current_status)
            json_data = json.dumps(cls.latest_status.__dict__)
            SocketIOAgent.updateAMRState(json_data)

    @classmethod
    def update_state_by_road_detection(cls, road_state):
        cls.road_state = road_state
        json_data = json.dumps(road_state)
        SocketIOAgent.updateRoadState(json_data)