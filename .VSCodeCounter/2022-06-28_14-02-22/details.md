# Details

Date : 2022-06-28 14:02:22

Directory /home/jihungmycena/Documents/Mycena_Project/YLM/developing/warehourse_poc/backend

Total : 33 files,  638 codes, 33 comments, 134 blanks, all 805 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.idea/backend.iml](/.idea/backend.iml) | XML | 10 | 0 | 0 | 10 |
| [.idea/inspectionProfiles/Project_Default.xml](/.idea/inspectionProfiles/Project_Default.xml) | XML | 53 | 0 | 0 | 53 |
| [.idea/inspectionProfiles/profiles_settings.xml](/.idea/inspectionProfiles/profiles_settings.xml) | XML | 6 | 0 | 0 | 6 |
| [.idea/misc.xml](/.idea/misc.xml) | XML | 4 | 0 | 0 | 4 |
| [.idea/modules.xml](/.idea/modules.xml) | XML | 8 | 0 | 0 | 8 |
| [.idea/vcs.xml](/.idea/vcs.xml) | XML | 6 | 0 | 0 | 6 |
| [agent/__init__.py](/agent/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [agent/authentication.py](/agent/authentication.py) | Python | 9 | 0 | 4 | 13 |
| [agent/ros_get_state.py](/agent/ros_get_state.py) | Python | 85 | 0 | 11 | 96 |
| [agent/socketio_agent.py](/agent/socketio_agent.py) | Python | 30 | 1 | 9 | 40 |
| [agent/sql_lite_db.py](/agent/sql_lite_db.py) | Python | 28 | 1 | 7 | 36 |
| [app.py](/app.py) | Python | 39 | 3 | 13 | 55 |
| [config.py](/config.py) | Python | 7 | 0 | 3 | 10 |
| [controller/__init__.py](/controller/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [controller/amr_status_con.py](/controller/amr_status_con.py) | Python | 11 | 0 | 4 | 15 |
| [controller/ros_api_con.py](/controller/ros_api_con.py) | Python | 116 | 1 | 19 | 136 |
| [controller/section_state_con.py](/controller/section_state_con.py) | Python | 21 | 27 | 9 | 57 |
| [controller/sys_log_con.py](/controller/sys_log_con.py) | Python | 10 | 0 | 4 | 14 |
| [database.sqlite](/database.sqlite) | SQL | 21 | 0 | 0 | 21 |
| [domain/__init__.py](/domain/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [domain/amr_status.py](/domain/amr_status.py) | Python | 42 | 0 | 3 | 45 |
| [domain/section_state.py](/domain/section_state.py) | Python | 5 | 0 | 2 | 7 |
| [domain/sys_log.py](/domain/sys_log.py) | Python | 13 | 0 | 5 | 18 |
| [repository/__init__.py](/repository/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [repository/section_state_rep.py](/repository/section_state_rep.py) | Python | 18 | 0 | 9 | 27 |
| [repository/sys_log_rep.py](/repository/sys_log_rep.py) | Python | 15 | 0 | 5 | 20 |
| [service/__init__.py](/service/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [service/amr_status_service.py](/service/amr_status_service.py) | Python | 5 | 0 | 1 | 6 |
| [service/section_state_service.py](/service/section_state_service.py) | Python | 29 | 0 | 8 | 37 |
| [service/sys_log_service.py](/service/sys_log_service.py) | Python | 10 | 0 | 5 | 15 |
| [stores/__init__.py](/stores/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [stores/state.py](/stores/state.py) | Python | 33 | 0 | 5 | 38 |
| [test.py](/test.py) | Python | 4 | 0 | 2 | 6 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)