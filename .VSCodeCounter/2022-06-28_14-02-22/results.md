# Summary

Date : 2022-06-28 14:02:22

Directory /home/jihungmycena/Documents/Mycena_Project/YLM/developing/warehourse_poc/backend

Total : 33 files,  638 codes, 33 comments, 134 blanks, all 805 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 26 | 530 | 33 | 134 | 697 |
| XML | 6 | 87 | 0 | 0 | 87 |
| SQL | 1 | 21 | 0 | 0 | 21 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 33 | 638 | 33 | 134 | 805 |
| .idea | 6 | 87 | 0 | 0 | 87 |
| .idea/inspectionProfiles | 2 | 59 | 0 | 0 | 59 |
| agent | 5 | 152 | 2 | 32 | 186 |
| controller | 5 | 158 | 28 | 37 | 223 |
| domain | 4 | 60 | 0 | 11 | 71 |
| repository | 3 | 33 | 0 | 15 | 48 |
| service | 4 | 44 | 0 | 15 | 59 |
| stores | 2 | 33 | 0 | 6 | 39 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)