import sqlite3
import config as cfg
from flask import g


class DBAgent:

    @classmethod
    def init_db(cls):
        qstr = "SELECT name FROM sqlite_master WHERE type='table';"
        result = [d[0] for d in cls.query_db(qstr)]
        if "sys_log" not in result:
            create_table_qstr = "create table sys_log (create_date date, content varchar)"
            cls.query_db(create_table_qstr)

        if "section_state" not in result:
            create_table_qstr = "create table section_state (section_id int primary key unique, state boolean)"
            cls.query_db(create_table_qstr)

    @classmethod
    def get_db(cls):
        db = getattr(g, '_database', None)
        if db is None:
            db = g._database = sqlite3.connect(cfg.DBFile)
            # Enable foreign key check
            db.execute("PRAGMA foreign_keys = ON")
        return db

    @classmethod
    def query_db(cls, query, args=(), one=False):
        cur = cls.get_db().execute(query, args)
        rv = cur.fetchall()
        cls.get_db().commit()
        cur.close()
        return (rv[0] if rv else None) if one else rv
