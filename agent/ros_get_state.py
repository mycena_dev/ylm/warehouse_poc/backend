import logging
import rospy
from tm_tech_mission_manager.msg import MissionManagerStatus
from tm_tech_amr_state_agent.msg import TmTechAmrState
from tm_tech_moving_agent.msg import TmTechMovingAgent
from std_msgs.msg import String
from sensor_msgs.msg import LaserScan
from stores.state import StatusStore
from domain.section_state import SectionState
from service import section_state_service
import time


class RosTopicBridge:
    app = None
    logger = logging.getLogger()
    is_init = False
    lidar_count = 0
    pallet_section = [2,4,11,13,15,19,21,23]
    presneak_section = [3,5,10,12,14,18,20,22]
    skip_count = 5
    mission_manager_status_count = 0
    current_moving_agent_status_count = 0
    push_rod_state_count = 0
    amr_state_count =0
    scan_count = 0

    @classmethod
    def set_logger(cls, logger):
        cls.logger = logger

    @classmethod
    def set_app(cls, app):
        cls.app = app

    @classmethod
    def init_bridge(cls):
        if not cls.is_init:
            cls.is_init = True
            import threading
            threading.Thread(target=cls.broadcast, daemon=True).start()

    @classmethod
    def broadcast(cls):
        print("===========Init Ros Node.....===========")
        rospy.init_node('get_all_status', disable_signals=True)
        print("===========Init Ros Node.....Done===========")
        rate = rospy.Rate(2)
        rospy.Subscriber("mission_manager_status", MissionManagerStatus, cls.cb_mission_manager_status)
        rate.sleep()
        print("===========Subscribe mission_manager_status.....Done===========")

        rospy.Subscriber("current_moving_agent_status", TmTechMovingAgent, cls.cb_moving_agent_status)
        rate.sleep()
        print("===========Subscribe current_moving_agent_status.....Done===========")

        rospy.Subscriber("push_rod_state", String, cls.cb_push_rod_state)
        rate.sleep()
        print("===========Subscribe push_rod_state.....Done===========")

        rospy.Subscriber("amr_state", TmTechAmrState, cls.cb_amr_state)
        rate.sleep()
        print("===========Subscribe amr_state.....Done===========")

        rospy.Subscriber("scan", LaserScan, cls.cb_check_lidar_state)
        rate.sleep()
        print("===========Subscribe scan.....Done===========")

        print("=============Success to subscribe all ros topic================")
        while not rospy.is_shutdown():
            # print("=============Time to broadcast data================")
            StatusStore.update_state_by_ros_bridge_trigger()
            rate.sleep()


    '''
    mission_manager_state: idle, busy, error
    mission_manager_gate_request: need_open/1f, need_open/2f, need_open/3f, need_close/1f, need_close/2f, need_close/3f, none
    mission_manager_current_route_path_array_index: 0,1,2,3,4,(int)
    mission_manager_route_path: [1,2,3,4,5,6,7,8]
    '''
    #palletSectionNum = 2, 4, 11, 13, 15, 19, 21, 23
    @classmethod
    def cb_mission_manager_status(cls, data):
        if cls.mission_manager_status_count < cls.skip_count:
            cls.mission_manager_status_count += 1
        else:
            cls.mission_manager_status_count = 0
            StatusStore.current_status.mission_manager_state = data.state
            StatusStore.current_status.mission_manager_gate_request = data.gateRequest
            StatusStore.current_status.mission_manager_current_route_path_array_index = data.currentRoutePathArrayIndex
            StatusStore.current_status.mission_manager_route_path = data.routePath
            # StatusStore.update_state_by_ros_bridge_trigger()
            current_index = data.currentRoutePathArrayIndex
            routePath = data.routePath
            if current_index > 0 and len(routePath) > 0:
                if routePath[current_index] in cls.presneak_section and routePath[current_index-1] in cls.pallet_section:
                    if StatusStore.current_status.push_rod_state == "down":
                        with cls.app.app_context():
                            section_state_service.update(SectionState(section_id=routePath[current_index-1], state=True))
                    else:
                        with cls.app.app_context():
                            section_state_service.update(SectionState(section_id=routePath[current_index-1], state=False))

    '''
    moving_agent_current_position: 'standby_position'
    moving_agent_path_num: ignore
    '''
    @classmethod
    def cb_moving_agent_status(cls, data):
        if cls.current_moving_agent_status_count < cls.skip_count:
            cls.current_moving_agent_status_count += 1
        else:
            cls.current_moving_agent_status_count = 0
            StatusStore.current_status.moving_agent_current_position = data.current_position
            StatusStore.current_status.moving_agent_path_num = data.path_num
            # StatusStore.update_state_by_ros_bridge_trigger()

    '''
    push_rod_state: up/ down
    '''
    @classmethod
    def cb_push_rod_state(cls, data):
        if cls.push_rod_state_count < cls.skip_count:
            cls.push_rod_state_count += 1
        else:
            cls.push_rod_state_count = 0
            StatusStore.current_status.push_rod_state = data.data
            # StatusStore.update_state_by_ros_bridge_trigger()

    '''
    amr_state_battery_state:  (int) value, 33 is 100%, over 50 is charging
    lrf_f_enable: (boolean) true is on.
    lrf_f_state: (int)
    lrf_b_enable: (boolean) true is on.
    lrf_b_state: (int)
    bumper_enable: (boolean) true is on.
    bumper_hit: (boolean) true is on.
    error_code: (string)
    velocity_x: float64,  1.5 is max
    angular_z: float64, 0.8 is max
    charge_state: (boolean) true is on.
    agv_status: string
    '''
    @classmethod
    def cb_amr_state(cls, data):
        if cls.amr_state_count < cls.skip_count:
            cls.amr_state_count += 1
        else:
            cls.amr_state_count = 0
            StatusStore.current_status.amr_state_battery_state = data.battery_state
            StatusStore.current_status.amr_state_lrf_f_enable = data.lrf_f_enable
            StatusStore.current_status.amr_state_lrf_f_state = data.lrf_f_state
            StatusStore.current_status.amr_state_lrf_b_enable = data.lrf_b_enable
            StatusStore.current_status.amr_state_lrf_b_state = data.lrf_b_state
            StatusStore.current_status.amr_state_bumper_enable = data.bumper_enable
            StatusStore.current_status.amr_state_bumper_hit = data.bumper_hit
            StatusStore.current_status.amr_state_error_code = data.error_code
            StatusStore.current_status.amr_state_velocity_x = data.velocity_x
            StatusStore.current_status.amr_state_angular_z = data.angular_z
            StatusStore.current_status.amr_state_charge_state = data.charge_state
            StatusStore.current_status.amr_state_agv_status = data.agv_status
            cls.lidar_count += 1
            if cls.lidar_count > 100:
                StatusStore.current_status.arm_lidar_state = False
            # StatusStore.update_state_by_ros_bridge_trigger()

    @classmethod
    def cb_check_lidar_state(cls, _):
        if cls.scan_count < cls.skip_count/2:
            cls.scan_count += 1
        else:
            cls.scan_count = 0
            cls.lidar_count = 0
            StatusStore.current_status.arm_lidar_state = True
