from flask_socketio import send, emit, SocketIO
import config
import json

# from flask import request


class SocketIOAgent:
    socketIO: SocketIO = None

    def __init__(self, app):
        if SocketIOAgent.socketIO is None:
            SocketIOAgent.socketIO = SocketIO(app, cors_allowed_origins="*", engineio_logger=False, log_output=False, debug=False)
            @SocketIOAgent.socketIO.on('RoadState')
            def handle_road_state_message(data):
                road_state = json.loads(data)
                new_road_state = {"floor1": road_state["1F"], "floor2": road_state["2F"], "floor3": road_state["3F"] }
                from stores.state import StatusStore
                StatusStore.update_state_by_road_detection(new_road_state)

            @SocketIOAgent.socketIO.on('connect')
            def connect():
                from stores.state import StatusStore
                StatusStore.broadcastAllStatusFromStore()


    @classmethod
    def updateAMRState(cls, data):
        if not config.BroadcastStoreToSocketIO:
            return
        cls.socketIO.emit("AMRState", data, broadcast=True)

    @classmethod
    def updateSectionState(cls, data):
        if not config.BroadcastStoreToSocketIO:
            return
        cls.socketIO.emit("SectionState", data, broadcast=True)

    @classmethod
    def updateLogMessage(cls, data):
        if not config.BroadcastStoreToSocketIO:
            return
        cls.socketIO.emit("EventLog", data, broadcast=True)

    @classmethod
    def updateRoadState(cls, data):
        if not config.BroadcastStoreToSocketIO:
            return
        cls.socketIO.emit("RoadState", data, broadcast=True)

