class Authentication:
    allowToken = {}

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        method = environ.get('REQUEST_METHOD')
        if method == "OPTIONS":
            return self.app(environ, start_response)

        return self.app(environ, start_response)
