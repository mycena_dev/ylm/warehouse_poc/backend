from domain.sys_log import SysLog
from agent.sql_lite_db import DBAgent


def get_last_500():
    qstr = f'SELECT * FROM {SysLog.table_name} ORDER BY create_date ASC LIMIT 500'
    result = DBAgent.query_db(qstr)
    logs = []
    for d in result:
        single_log = SysLog(create_date=d[0], content=d[1])
        logs.append(single_log)
    return logs


def insert_one(log_data):
    qstr = f'insert into {SysLog.table_name} (create_date,content) values ("{log_data.create_date}", "{log_data.content}")'
    result = DBAgent.query_db(qstr)
    return result
