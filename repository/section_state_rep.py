from domain.section_state import SectionState
from agent.sql_lite_db import DBAgent


def get_all():
    qstr = f'SELECT * FROM {SectionState.table_name}'
    result = DBAgent.query_db(qstr)
    logs = []
    for d in result:
        single_log = SectionState(section_id=d[0], state=(False if d[1] == "False" else True))
        logs.append(single_log)
    return logs


def insert_one(section_data):
    qstr = f'insert into {SectionState.table_name} (section_id,state) values ("{section_data.section_id}", "{section_data.state}")'
    result = DBAgent.query_db(qstr)
    return result


def update_one(section_data):
    qstr = f'update {SectionState.table_name} set state="{section_data.state}" where section_id == "{section_data.section_id}"'
    result = DBAgent.query_db(qstr)
    return result


