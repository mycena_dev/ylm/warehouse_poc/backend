from typing import List


class AMRStatus:
    def __init__(self, mission_manager_state = "idle", 
                mission_manager_gate_request = "none",
                mission_manager_current_route_path_array_index=0, 
                mission_manager_route_path=[],
                moving_agent_current_position="standby_position", 
                moving_agent_path_num=0, 
                push_rod_state="down",
                amr_state_battery_state=0, 
                amr_state_lrf_f_enable=False, 
                amr_state_lrf_f_state=0,
                amr_state_lrf_b_enable=False, 
                amr_state_lrf_b_state=0, 
                amr_state_bumper_enable=False,
                amr_state_bumper_hit=False, 
                amr_state_error_code="\x00", 
                amr_state_velocity_x=0.0,
                amr_state_angular_z=0.0,
                amr_state_charge_state=False, 
                amr_state_agv_status="ok", 
                arm_lidar_state=False):
        self.mission_manager_state = mission_manager_state
        self.mission_manager_gate_request = mission_manager_gate_request
        self.mission_manager_current_route_path_array_index = mission_manager_current_route_path_array_index
        self.mission_manager_route_path = mission_manager_route_path
        self.moving_agent_current_position = moving_agent_current_position
        self.moving_agent_path_num = moving_agent_path_num
        self.push_rod_state = push_rod_state
        self.amr_state_battery_state = amr_state_battery_state
        self.amr_state_lrf_f_enable = amr_state_lrf_f_enable
        self.amr_state_lrf_f_state = amr_state_lrf_f_state
        self.amr_state_lrf_b_enable = amr_state_lrf_b_enable
        self.amr_state_lrf_b_state = amr_state_lrf_b_state
        self.amr_state_bumper_enable = amr_state_bumper_enable
        self.amr_state_bumper_hit = amr_state_bumper_hit
        self.amr_state_error_code = amr_state_error_code
        self.amr_state_velocity_x = amr_state_velocity_x
        self.amr_state_angular_z = amr_state_angular_z
        self.amr_state_charge_state = amr_state_charge_state
        self.amr_state_agv_status = amr_state_agv_status
        self.arm_lidar_state = arm_lidar_state

    def update(self, obj):
        self.mission_manager_state = obj.mission_manager_state
        self.mission_manager_gate_request = obj.mission_manager_gate_request
        self.mission_manager_current_route_path_array_index = obj.mission_manager_current_route_path_array_index
        self.mission_manager_route_path = obj.mission_manager_route_path
        self.moving_agent_current_position = obj.moving_agent_current_position
        self.moving_agent_path_num = obj.moving_agent_path_num
        self.push_rod_state = obj.push_rod_state
        self.amr_state_battery_state = obj.amr_state_battery_state
        self.amr_state_lrf_f_enable = obj.amr_state_lrf_f_enable
        self.amr_state_lrf_f_state = obj.amr_state_lrf_f_state
        self.amr_state_lrf_b_enable = obj.amr_state_lrf_b_enable
        self.amr_state_lrf_b_state = obj.amr_state_lrf_b_state
        self.amr_state_bumper_enable = obj.amr_state_bumper_enable
        self.amr_state_bumper_hit = obj.amr_state_bumper_hit
        self.amr_state_error_code = obj.amr_state_error_code
        self.amr_state_velocity_x = obj.amr_state_velocity_x
        self.amr_state_angular_z = obj.amr_state_angular_z
        self.amr_state_charge_state = obj.amr_state_charge_state
        self.amr_state_agv_status = obj.amr_state_agv_status
        self.arm_lidar_state = obj.arm_lidar_state