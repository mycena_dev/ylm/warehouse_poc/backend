class SectionState:
    table_name = "section_state"

    def __init__(self, section_id, state):
        self.section_id = section_id
        self.state = state
