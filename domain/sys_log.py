from datetime import datetime
import json


class SysLog:
    table_name = "sys_log"

    def __init__(self, create_date=None, content=""):
        if create_date is not None:
            self.create_date = create_date
        else:
            self.create_date = datetime.now().strftime("%B %d, %Y %I:%M%p")
        self.content = content

    def convert_to_json(self):
        sys_log_str = json.dumps({"create_date": self.create_date,
                                  "content": self.content
                                  },
                                 indent=4)
        return sys_log_str
