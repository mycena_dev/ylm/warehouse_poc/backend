from flask import Blueprint, request
from flask_api import status
from bson import json_util
from service import amr_status_service
from stores.state import StatusStore
import json

mod = Blueprint('amr_status_con', __name__, url_prefix='/amr')


@mod.route('status', methods=['GET'])
def get_section_state():
  json_data = json.dumps(amr_status_service.get_current_status_from_store())
  return json_data, status.HTTP_200_OK, {'ContentType': 'application/json'}
