from flask import Blueprint, request
from flask_api import status
from bson import json_util
from domain.section_state import SectionState
from service import section_state_service

mod = Blueprint('section_state_con', __name__, url_prefix='/section')


@mod.route('state', methods=['PUT'])
def update_section_state():
    """
        put endpoint
        ---
        tags:
          - sectionController
        parameters:
          - name: body
            in: body
            required: true
            schema:
              required:
                - sectionId
                - state
              properties:
                sectionId:
                  type: string
                  description: The section's Id.
                  default: "section2"
                state:
                  type: boolean
                  description: The section's state.
                  default: ""
        responses:
          200:
            description: OK
            schema:
        """
    data = request.get_json()
    isInputDataOK = "sectionId" in data and "state" in data
    if not isInputDataOK:
        return "Input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE
    section_id = data["sectionId"]
    state = data["state"]
    section_state_service.update(SectionState(section_id=section_id, state=state))

    return "OK", status.HTTP_200_OK


@mod.route('state', methods=['GET'])
def get_section_state():
    all_data = section_state_service.get_all()
    return json_util.dumps({'sections': all_data}), status.HTTP_200_OK, {
        'ContentType': 'application/json'}
