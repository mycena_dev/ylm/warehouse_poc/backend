import rospy
from tm_tech_mission_manager.srv import EmergencyStop, CreateMission, UpdateGateState
from std_srvs.srv import SetBool
from flask import Blueprint, request
from flask_api import status
from stores.state import StatusStore

mod = Blueprint('ros_api_con', __name__, url_prefix='/ros-service')


'''
CreateMission:
    string missionType #transport, go_charge, go_destination
    string startSectionId
    string endSectionId

EmergencyStop:
    None, just uew

UpdateGateState:
    string state # open, closed

`push_rod_controller`:
    SetBool # up: true,  down: false


'''

class Services:
    trigger_create_mission = rospy.ServiceProxy('createMission', CreateMission)
    trigger_emergency_stop = rospy.ServiceProxy('emergencyStop', EmergencyStop)
    trigger_reset_emergency_stop = rospy.ServiceProxy('resetEmergencyStop', EmergencyStop)
    trigger_update_gate_state = rospy.ServiceProxy('updateGateState', UpdateGateState)
    trigger_push_rod = rospy.ServiceProxy('push_rod_controller', SetBool)
    manual_stop_charge = rospy.ServiceProxy('manualStopCharge', SetBool)

@mod.route('transport', methods=['POST'])
def transport_command():
    data = request.get_json()
    print(data)
    # return "OK", status.HTTP_200_OK
    isInputDataOK = len(data['startSectionId']) > 0 and len(data['endSectionId']) > 0
    isAMRNotBusy = StatusStore.latest_status.mission_manager_state == 'idle'
    isAMRStateOK = StatusStore.latest_status.amr_state_error_code == '\x00'
    if not isAMRNotBusy or not isInputDataOK or not isAMRStateOK:
        return "AMR is busy OR input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE
    cmd = Services.trigger_create_mission('transport', data['startSectionId'], data['endSectionId'])
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE


@mod.route('go-charge', methods=['POST'])
def go_charge_command():
    print("go-charge")
    # return "OK", status.HTTP_200_OK
    cmd = Services.trigger_create_mission('go_charge', "charge_position", "charge_position")
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('stop-charge', methods=['POST'])
def stop_charge_command():
    print("stop-charge")
    # return "OK", status.HTTP_200_OK
    cmd = Services.manual_stop_charge(True)
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.".  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('go-destination', methods=['POST'])
def go_destination_command():
    data = request.get_json()
    print("go-destination")
    print(data)
    return "OK", status.HTTP_200_OK
    # isInputDataOK = len(data['endSectionId']) > 0
    # isAMRNotBusy = StatusStore.latest_status.mission_manager_state == 'idle'
    # isAMRStateOK = StatusStore.latest_status.amr_state_error_code == '\x00'
    # if not isAMRNotBusy or not isInputDataOK or not isAMRStateOK:
    #     return "AMR is busy OR input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE
    # cmd = Services.trigger_create_mission('go_destination', data['endSectionId'], data['endSectionId'])
    # if cmd.success:
    #     return "OK", status.HTTP_200_OK
    # else :
    #     return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('return', methods=['POST'])
def go_standby_position():
    print("go-standby-position")
    # return "OK", status.HTTP_200_OK
    isAMRNotBusy = StatusStore.latest_status.mission_manager_state == 'idle'
    isAMRStateOK = StatusStore.latest_status.amr_state_error_code == '\x00'
    if not isAMRNotBusy or not isAMRStateOK:
        return "AMR is busy OR input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE
    cmd = Services.trigger_create_mission('go_destination', "standby_position", "standby_position")
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('emergency-stop-method', methods=['POST'])
def emergency_stop_method():
    print("emergency_stop_method")
    # return "OK", status.HTTP_200_OK
    cmd = Services.trigger_emergency_stop()
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.".  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('reset-emergency-stop', methods=['POST'])
def reset_emergency_stop_method():
    print("reset_emergency_stop_method")
    # return "OK", status.HTTP_200_OK
    cmd = Services.trigger_reset_emergency_stop()
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.".  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('update-gate-state', methods=['POST'])
def update_gate_state_command():
    data = request.get_json()
    print(data)
    # return "OK", status.HTTP_200_OK
    isInputDataOK = len(data['state']) > 0
    isGateNeedUpdate = StatusStore.latest_status.mission_manager_gate_request != "none"
    if not isGateNeedUpdate or not isInputDataOK:
        return "Gate no need update OR input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE
    cmd = Services.trigger_update_gate_state(data['state'])
    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE

@mod.route('update-push-rod-state', methods=['POST'])
def update_push_rod_state_command():
    data = request.json
    # return "OK", status.HTTP_200_OK
    isInputDataOK = len(data['state']) > 0
    if not isInputDataOK:
        return "input data wrong.", status.HTTP_503_SERVICE_UNAVAILABLE

    cmd = Services.trigger_push_rod(data['state']=='up')

    if cmd.success:
        return "OK", status.HTTP_200_OK
    else :
        return "Getting false signal.",  status.HTTP_503_SERVICE_UNAVAILABLE

        # 192.168.1.148~150