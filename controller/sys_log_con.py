from flask import Blueprint
from flask_api import status
from bson import json_util
from service import sys_log_service
from stores.state import StatusStore
mod = Blueprint('sys_log', __name__, url_prefix='/sys-log')


@mod.route('', methods=['GET'])
def get_latest_500():
    all_data = sys_log_service.get_latest_500()
    return json_util.dumps({'logs': [ob.__dict__ for ob in all_data]}), status.HTTP_200_OK, {
        'ContentType': 'application/json'}


@mod.route('/store', methods=['GET'])
def manualBroad0castStore():
    StatusStore.broadcastAllStatusFromStore()
    return 'ok', 200


