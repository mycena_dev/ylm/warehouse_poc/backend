from flask import Flask, make_response, redirect
from flask_cors import CORS
from flasgger import Swagger
from agent.socketio_agent import SocketIOAgent
from agent.authentication import Authentication
from flask import g
from service import section_state_service
import config
from stores.state import StatusStore
from agent.ros_get_state import RosTopicBridge
import logging

logging.basicConfig(
    filename='system_backend.log',
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

# init system needed object and class
app = Flask(__name__, static_folder="./dist", static_url_path="/")
swag = Swagger(app)
SocketIOAgent(app)
CORS(app)
app.config.from_object('config')
app.wsgi_app = Authentication(app.wsgi_app)

with app.app_context():
    from agent.sql_lite_db import DBAgent

    DBAgent.init_db()
    section_state_service.init_database()

    #init store
    StatusStore.initStore()

    @app.teardown_appcontext
    def close_connection(exception):
        db = getattr(g, '_database', None)
        if db is not None:
            db.close()

from controller.ros_api_con import mod as amr_module
app.register_blueprint(amr_module)

from controller.sys_log_con import mod as sys_log_module
app.register_blueprint(sys_log_module)

from controller.amr_status_con import mod as amr_status_module
app.register_blueprint(amr_status_module)

from controller.section_state_con import mod as section_state_con
app.register_blueprint(section_state_con)

@app.route('/', methods=['GET'])
def root():
    return redirect("/floor_01", code=302)
    
@app.route('/bug', methods=['GET'])
def logs():
    return make_response(open('./dist/bug/index.html').read())

@app.route('/floor_01', methods=['GET'])
def floor_01():
    return make_response(open('./dist/floor_01/index.html').read())

@app.route('/floor_02', methods=['GET'])
def floor_02():
    return make_response(open('./dist/floor_02/index.html').read())

@app.route('/floor_03', methods=['GET'])
def floor_03():
    return make_response(open('./dist/floor_03/index.html').read())

@app.route('/agv', methods=['GET'])
def agv():
    return make_response(open('./dist/agv/index.html').read())


if __name__ == '__main__':
    # app.logger.setLevel(logging.DEBUG)
    RosTopicBridge.set_app(app=app)
    RosTopicBridge.set_logger(app.logger)
    RosTopicBridge.init_bridge()
    app.run(host="0.0.0.0", port=1111, use_reloader=False, threaded=True)
