from domain.sys_log import SysLog
import repository.sys_log_rep as rep
from agent.socketio_agent import SocketIOAgent


def get_latest_500():
    result = rep.get_last_500()
    return result


def insert_one(sys_log):
    result = rep.insert_one(sys_log)
    SocketIOAgent.updateLogMessage(sys_log.convert_to_json())
    return result
