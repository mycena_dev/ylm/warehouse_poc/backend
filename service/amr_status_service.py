from domain.amr_status import AMRStatus
from stores.state import StatusStore

def get_current_status_from_store(): 
    result = StatusStore.current_status
    return result