from domain.sys_log import SysLog
import repository.section_state_rep as rep
from domain.section_state import SectionState
from config import all_section_id
from service import sys_log_service

def get_all():
    existed_section = rep.get_all()
    all_section_state = {}
    for section in existed_section:
        all_section_state[ section.section_id]  = section.state
    return all_section_state


def get_all_from_store():
    from stores.state import StatusStore
    return StatusStore.section_status


def update(section_state):
    if section_state is None:
        return None
    from stores.state import StatusStore
    if StatusStore.section_status[section_state.section_id] == section_state.state:
        return section_state
    result = rep.update_one(section_state)
    StatusStore.update_state_by_section_state_change(section_state)
    log_str = "Section state has change! %s's state of having box becomes %s" % \
              (section_state.section_id, str(section_state.state))
    sys_log_service.insert_one(SysLog(content=log_str))
    return result


def init_database():
    existed_section = rep.get_all()
    existed_section_id = [section.section_id for section in existed_section]
    for section_id in all_section_id:
        if section_id not in existed_section_id:
            rep.insert_one(SectionState(section_id, False))
